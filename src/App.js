import React, { useState } from 'react';


export default function App() {
	const questions = [
		{
			questionText: 'How many legs does a spider have?',
			answerOptions: [
				{ answerText: '6', isCorrect: false },
				{ answerText: '10', isCorrect: false },
				{ answerText: '8', isCorrect: true },
				{ answerText: '7', isCorrect: false },
			],
		},
		{
			questionText: 'How long do elephant pregnancies last?',
			answerOptions: [
				{ answerText: '24 months', isCorrect: false },
				{ answerText: '22 months', isCorrect: true },
				{ answerText: '14 months', isCorrect: false },
				{ answerText: '18 months', isCorrect: false },
			],
		},
		{
			questionText: 'How many hearts does an octopus have?',
			answerOptions: [
				{ answerText: '1', isCorrect: false },
				{ answerText: '2', isCorrect: false },
				{ answerText: '3', isCorrect: true },
				{ answerText: '4', isCorrect: false },
			],
		},
		{
			questionText: 'What color is the tongue of a giraffe?',
			answerOptions: [
				{ answerText: 'Pink', isCorrect: false },
				{ answerText: 'Red', isCorrect: false },
				{ answerText: 'Blue', isCorrect: false },
				{ answerText: 'Purple', isCorrect: true },
			],
		},
		{
			questionText: 'How long does it take a sloth to digest a meal?',
			answerOptions: [
				{ answerText: '2 days', isCorrect: false },
				{ answerText: '1 day', isCorrect: false },
				{ answerText: '2 weeks', isCorrect: true },
				{ answerText: '1 week', isCorrect: false },
			],
		},
		{
			questionText: 'How many noses does a slug have?',
			answerOptions: [
				{ answerText: '1', isCorrect: false },
				{ answerText: '2', isCorrect: false },
				{ answerText: '3', isCorrect: false },
				{ answerText: '4', isCorrect: true },
			],
		},
		{
			questionText: 'How many eyes does a shrimp have?',
			answerOptions: [
				{ answerText: ' 12-16', isCorrect: true },
				{ answerText: '8-10', isCorrect: false },
				{ answerText: '4-6', isCorrect: false },
				{ answerText: '18-20', isCorrect: false },
			],
		},
		{
			questionText: 'How many times does a hummingbird flap its wings to hover?',
			answerOptions: [
				{ answerText: '100 times per second', isCorrect: false },
				{ answerText: '10 times per second', isCorrect: false },
				{ answerText: '50 times per second', isCorrect: false },
				{ answerText: '200 times per second', isCorrect: true },
			],
		},
		
		
	];


	const [currentQuestion, setCurrentQuestion] = useState(0);
	const [showScore, setShowScore] = useState(false);
	const [score, setScore] = useState(0);

	const handleAnswerOptionClick = (isCorrect) => {
		if (isCorrect) {
			setScore(score + 1);
		}

		const nextQuestion = currentQuestion + 1;
		if (nextQuestion < questions.length) {
			setCurrentQuestion(nextQuestion);
		} else {
			setShowScore(true);
		}
	};
	return (
		<div className='app'>
			{showScore ? (
				<div className='score-section'>
					You scored {score} out of {questions.length}
				</div>
			) : (
				<>
					<div className='question-section'>
						<div className='question-count'>
							<span>Question {currentQuestion + 1}</span>/{questions.length}
						</div>
						<div className='question-text'>{questions[currentQuestion].questionText}</div>
					</div>
					<div className='answer-section'>
						{questions[currentQuestion].answerOptions.map((answerOption) => (
							<button onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
						))}
					</div>
				</>
			)}
		</div>
	);
}
